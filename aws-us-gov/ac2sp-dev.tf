terraform {
  backend "s3" {}
}

provider "aws" {
  version = "~> 1.58.0"

  allowed_account_ids = ["${lookup(var.account_ids[var.partition], "${var.name}-${var.stage}")}"]
  profile             = "ac2sp-na2d"
  region              = "${var.region}"

  assume_role {
    role_arn = "arn:${var.partition}:iam::${lookup(var.account_ids[var.partition], "${var.name}-${var.stage}")}:role/${var.role_name}"
  }
}

provider "aws" {
  version = "~> 1.58.0"

  alias = "parent"

  allowed_account_ids = ["${lookup(var.account_ids[var.partition], var.parent_name)}"]
  profile             = "ac2sp-na2d"
  region              = "${var.region}"
}

variable "region" {
  description = "The region where resources are being deployed"
  type        = "string"
}

variable "account_ids" {
  description = "Map of partitions and account names to account IDs"
  type        = "map"
}

variable "partition" {
  description = "AWS Partition where account exists"
  type        = "string"
}

variable "role_name" {
  description = "Name of the role that will be assumed by terraform"
  type        = "string"
}

variable "parent_name" {
  description = "Name of the management account"
  type        = "string"
}

variable "name" {
  description = "Name of the tenant account"
  type        = "string"
}
