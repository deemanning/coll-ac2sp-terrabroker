# name of the account, i.e. "mock"
name = "iam-management"

# stage for the account, i.e. dev, test, prod, etc...
stage = "dev"

custom_roles   = {
  "iam"      = []
  "instance" = []
}
