region = "us-gov-west-1"

partition = "aws-us-gov"

role_name = "AC2SPMGT"

parent_name = "ac2sp-iam"

template_vars  = {
  partition = "aws-us-gov"
  log_bucket_id = "ac2sp-logs"
}

account_roles  = [
  {
    name   = "SERVADMIN"
    policy = "policies/SERVADMIN.template.json"
    trust  = "trusts/CROSS_ACCOUNT.template.json"
  },
  {
    name   = "BUSINESS"
    policy = "policies/BUSINESS.template.json"
    trust  = "trusts/CROSS_ACCOUNT.template.json"
  },
  {
    name   = "CNDIS"
    policy = "policies/CNDIS.template.json"
    trust  = "trusts/CROSS_ACCOUNT.template.json"
  },
  {
    name   = "CNDISREADONLY"
    policy = "policies/CNDISREADONLY.template.json"
    trust  = "trusts/CROSS_ACCOUNT.template.json"
  },
  {
    name   = "CODEDEPLOYSERVICE"
    policy = "policies/CODEDEPLOYSERVICE.template.json"
    trust  = "trusts/CODEDEPLOY.template.json"
  },
  {
    name   = "CONFIG"
    policy = "policies/CONFIG.template.json"
    trust  = "trusts/CONFIG.template.json"
  },
  {
    name   = "CONFIGMANAGER"
    policy = "policies/CONFIGMANAGER.template.json"
    trust  = "trusts/CROSS_ACCOUNT.template.json"
  },
  {
    name   = "DATABASE"
    policy = "policies/DATABASE.template.json"
    trust  = "trusts/CROSS_ACCOUNT.template.json"
  },
  {
    name   = "EMRSERVICE"
    policy = "policies/EMRSERVICE.template.json"
    trust  = "trusts/elasticmapreduce.template.json"
  },
  {
    name   = "KEYMANAGER"
    policy = "policies/KEYMANAGER.template.json"
    trust  = "trusts/CROSS_ACCOUNT.template.json"
  },
  {
    name   = "MARKETPLACE"
    policy = "policies/MARKETPLACE.template.json"
    trust  = "trusts/CROSS_ACCOUNT.template.json"
  },
  {
    name   = "NETADMIN"
    policy = "policies/NETADMIN.template.json"
    trust  = "trusts/CROSS_ACCOUNT.template.json"
  },
  {
    name   = "PROJADMIN"
    policy = "policies/PROJADMIN.template.json"
    trust  = "trusts/CROSS_ACCOUNT.template.json"
  },
  {
    name   = "PROJADMINLIMITED"
    policy = "policies/PROJADMINLIMITED.template.json"
    trust  = "trusts/CROSS_ACCOUNT.template.json"
  },
  {
    name   = "S3ONLY"
    policy = "policies/S3ONLY.template.json"
    trust  = "trusts/CROSS_ACCOUNT.template.json"
  },
  {
    name   = "PROVADMIN"
    policy = "policies/PROVADMIN.template.json"
    trust  = "trusts/CROSS_ACCOUNT.template.json"
  },
  {
    name   = "TECHREADONLY"
    policy = "policies/TECHREADONLY.template.json"
    trust  = "trusts/CROSS_ACCOUNT.template.json"
  },
]

instance_roles = [
  {
    name   = "INSTANCE_EXAMPLE"
    policy = "policies/INSTANCE_EXAMPLE.template.json"
    trust  = "trusts/EC2.template.json"
  },
  {
    name   = "INSTANCE"
    policy = "policies/INSTANCE.template.json"
    trust  = "trusts/EC2.template.json"
  },
  {
    name   = "INSTANCECODEDEPLOY"
    policy = "policies/INSTANCECODEDEPLOY.template.json"
    trust  = "trusts/EC2.template.json"
  },
  {
    name   = "INSTANCEEMR"
    policy = "policies/INSTANCE.template.json"
    trust  = "trusts/EC2.template.json"
  },
]

parent_roles   = []
