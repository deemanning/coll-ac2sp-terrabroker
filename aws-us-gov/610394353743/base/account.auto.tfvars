# name of the account, i.e. "mock"
name = "uc2s-army-it"

# stage for the account, i.e. dev, test, prod, etc...
stage = "dev"

custom_roles   = {
  "iam"      = []
  "instance" = []
}
