terragrunt = {
  # Include all settings from the root terraform.tfvars file
  include = {
    path = "${find_in_parent_folders()}"
  }

  terraform = {
    source = "git::https://bitbucket.di2e.net/scm/armyaws/ac2sp-terrabroker-iam.git//modules/common?ref=master"

    after_hook "provider" {
      commands = ["init-from-module"]
      execute  = ["cp", "${get_tfvars_dir()}/../../ac2sp-dev.tf", "."]
    }

    extra_arguments "enclave" {
      commands = ["${get_terraform_commands_that_need_vars()}"]

      arguments = [
        "-var-file",
        "${get_tfvars_dir()}/../../ac2sp-dev.tfvars",
      ]
    }
  }
}
