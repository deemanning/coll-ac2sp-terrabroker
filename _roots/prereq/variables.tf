variable "stage" {
  description = "Stage for the account; used to construct the path to the baseline templates"
  type        = "string"
}

variable "template_vars" {
  description = "Map of input variables for IAM trust and policy templates."
  type        = "map"
}
