locals {
  prereq_roles = [
    {
      name   = "AC2SPMGT"
      policy = "policies/AC2SPMGT.template.json"
      trust  = "trusts/CROSS_ACCOUNT.template.json"
    },
  ]
}

data "aws_caller_identity" "this" {}

data "aws_caller_identity" "parent" {
  provider = "aws.parent"
}

module "iam_roles" {
  source = "git::https://bitbucket.di2e.net/scm/armyaws/terrabroker-modules.git//modules/iam_roles?ref=0.21.0"

  providers = {
    aws        = "aws"
  }

  roles           = "${local.prereq_roles}"
  template_vars   = "${var.template_vars}"
  template_path   = "${path.module}"
}
