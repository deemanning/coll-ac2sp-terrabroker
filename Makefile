ARCH ?= amd64
OS ?= $(shell uname -s | tr '[:upper:]' '[:lower:'])
CURL ?= curl --fail -sSL
XARGS ?= xargs -I {}
BIN_DIR ?= ${HOME}/bin

empty :=
space := $(empty) $(empty)

RUBY_VERSION ?= 2.4.1
RUBY_BIN_DIR ?= /usr/share/rvm/rubies/ruby-$(RUBY_VERSION)/bin
PATH := $(BIN_DIR):$(RUBY_BIN_DIR):$(PATH)

MAKEFLAGS += --no-print-directory
SHELL := bash
.SHELLFLAGS := -eu -o pipefail -c
.SUFFIXES:

.PHONY: %/install %/lint %/format
.PHONY: guard/% release/%

guard/env/%:
	@ _=$(or $($*),$(error Make/environment variable '$*' not present))

guard/program/%:
	@ which $* > /dev/null || $(MAKE) $*/install

$(BIN_DIR):
	@ echo "[make]: Creating directory '$@'..."
	mkdir -p $@

GITHUB_ACCESS_TOKEN ?= 4224d33b8569bec8473980bb1bdb982639426a92
# Macro to return the download url for a github release
# For latest release, use version=latest
# To pin a release, use version=tags/<tag>
# $(call parse_github_download_url,owner,repo,version,asset)
parse_github_download_url = $(CURL) https://api.github.com/repos/$(1)/$(2)/releases/$(3)?access_token=$(GITHUB_ACCESS_TOKEN) | jq --raw-output  '.assets[] | select(.name=="$(4)") | .browser_download_url'

# Macro to download a github binary release
# $(call download_github_release,file,owner,repo,version,asset)
download_github_release = $(CURL) -o $(1) $(shell $(call parse_github_download_url,$(2),$(3),$(4),$(5)))

# Macro to download a hashicorp archive release
# $(call download_hashicorp_release,file,app,version)
download_hashicorp_release = $(CURL) -o $(1) https://releases.hashicorp.com/$(2)/$(3)/$(2)_$(3)_$(OS)_$(ARCH).zip

pipenv/install: | guard/program/pip
	@ echo "[$@]: Installing $(@D)..."
	pip install pipenv
	@ echo "[$@]: Completed successfully!"

tb/install: | guard/program/pipenv
	@ echo "[$@]: Installing $(@D)..."
	pipenv install
	@ echo "[$@]: Completed successfully!"

zip/install:
	@ echo "[$@]: Installing $(@D)..."
	apt-get install zip -y
	@ echo "[$@]: Completed successfully!"

jq/install: JQ_VERSION ?= jq-1.5
jq/install: JQ_URL ?= https://github.com/stedolan/jq/releases/download/$(JQ_VERSION)/jq-linux64
jq/install: | $(BIN_DIR)
	@ echo "[$@]: Installing $(@D)..."
	@ echo "[$@]: JQ_URL=$(JQ_URL)"
	$(CURL) -o $(BIN_DIR)/$(@D) "$(JQ_URL)"
	chmod +x $(BIN_DIR)/$(@D)
	$(@D) --version
	@ echo "[$@]: Completed successfully!"

terraform/install: TERRAFORM_VERSION ?= $(shell $(CURL) https://checkpoint-api.hashicorp.com/v1/check/terraform | jq -r -M '.current_version' | sed 's/^v//')
terraform/install: | $(BIN_DIR)
	@ echo "[$@]: Installing $(@D)..."
	$(call download_hashicorp_release,$(@D).zip,$(@D),$(TERRAFORM_VERSION))
	unzip $(@D).zip && rm -f $(@D).zip && chmod +x $(@D)
	mv $(@D) "$(BIN_DIR)"
	$(@D) --version
	@ echo "[$@]: Completed successfully!"

terragrunt/install: TERRAGRUNT_VERSION ?= latest
terragrunt/install: | $(BIN_DIR) guard/program/jq
	@ echo "[$@]: Installing $(@D)..."
	$(call download_github_release,$(BIN_DIR)/$(@D),gruntwork-io,$(@D),$(TERRAGRUNT_VERSION),$(@D)_$(OS)_$(ARCH))
	chmod +x $(BIN_DIR)/$(@D)
	$(@D) --version
	@ echo "[$@]: Completed successfully!"

node/install: NODE_VERSION ?= 8.x
node/install: NODE_SOURCE ?= https://deb.nodesource.com/setup_$(NODE_VERSION)
node/install:
	@ echo "[$@]: Installing $(@D)..."
	$(CURL) "$(NODE_SOURCE)" | bash -
	apt-get install nodejs -y
	npm --version
	@ echo "[$@]: Completed successfully!"

npm/install: node/install

semver/install: | guard/program/npm
	@ echo "[$@]: Installing $(@D)..."
	npm install -g semver
	@ echo "[$@]: Completed successfully!"

markdownlint/install: | guard/program/npm
	@ echo "[$@]: Installing $(@D)..."
	npm install -g markdownlint-cli
	markdownlint --version
	@ echo "[$@]: Completed successfully!"

make/install: MAKE_MAKE_VERSION ?= 4.1
make/install: MAKE_SOURCE ?= http://ftp.gnu.org/gnu/make/make-$(MAKE_MAKE_VERSION).tar.gz
make/install: | $(BIN_DIR)
	@echo "[make]: MAKE_SOURCE=$(MAKE_SOURCE)"
	$(CURL) -o make.tar.gz "$(MAKE_SOURCE)"
	tar xvf make.tar.gz
	cd make-* && ./configure --bindir=$(BIN_DIR) && sudo make install
	rm -rf make*
	which make
	make --version

ruby/install: .SHELLFLAGS := -e -o pipefail -c
ruby/install:
	apt-add-repository -y ppa:rael-gc/rvm
	apt-get update
	apt-get install rvm -y
	source /usr/share/rvm/scripts/rvm && rvm install $(RUBY_VERSION)
	ruby --version
	gem --version

landscape/install: | guard/program/gem
	gem install terraform_landscape
	landscape --version

gem/install: ruby/install

json/%: FIND_JSON := find . -not \( -name .terraform -prune \) -not \( -name .terragrunt-cache -prune \) -name '*.json' -type f
json/lint: | guard/program/jq
	@ echo "[$@]: Linting JSON files..."
	$(FIND_JSON) | $(XARGS) bash -c 'cmp {} <(jq --indent 4 -S . {}) || (echo "[{}]: Failed JSON Lint Test"; exit 1)'
	@ echo "[$@]: JSON files PASSED lint test!"

json/format: | guard/program/jq
	@ echo "[$@]: Formatting JSON files..."
	$(FIND_JSON) | $(XARGS) bash -c 'echo "$$(jq --indent 4 -S . "{}")" > "{}"'
	@ echo "[$@]: Successfully formatted JSON files!"

terraform/lint: | guard/program/terraform
	@ echo "[$@]: Linting Terraform files..."
	terraform fmt -check=true -diff=true
	@ echo "[$@]: Terraform files PASSED lint test!"

markdown/lint: | guard/program/markdownlint
	@ echo "[$@]: Linting markdown files..."
	markdownlint -c .markdownlint.yaml **/*.md
	@echo "[$@] Markdown files PASSED lint test!"

PRIOR_VERSION := $(shell git describe --abbrev=0 --tags 2> /dev/null)
RELEASE_VERSION := $(shell grep '^current_version' .bumpversion.cfg | sed 's/^.*= //')

release/test: | guard/program/semver
	@ echo "[$@]: Checking version tag (prior) against version file (release)..."
	@ echo "[$@]: PRIOR_VERSION = $(PRIOR_VERSION)"
	@ echo "[$@]: RELEASE_VERSION = $(RELEASE_VERSION)"
	semver -r '> $(PRIOR_VERSION)' '$(RELEASE_VERSION)' > /dev/null

release/tag:
	@ if $(MAKE) release/test; then \
		echo "[$@]: Releasing version $(RELEASE_VERSION)"; \
		git tag $(RELEASE_VERSION); \
		git push --tags; \
	else \
		echo "[$@]: NOTE: An error message is expected when release/test displays the same versions."; \
		echo "[$@]: Version has not incremented, skipping tag release"; \
	fi

govprofile/create: TYPE := $(if $(IS_TAG),write,read)
govprofile/create: GOV_ACCESS_KEY_ID_SSM := /terrabroker-ci/aws-us-gov/terrabroker-ci-$(TYPE)/access_key
govprofile/create: GOV_SECRET_KEY_SSM := /terrabroker-ci/aws-us-gov/terrabroker-ci-$(TYPE)/secret_key
govprofile/create: GET_ACCESS_KEY_ID = $$(aws ssm get-parameters --names $(GOV_ACCESS_KEY_ID_SSM) --with-decryption | jq -e -c -r '.Parameters[0].Value')
govprofile/create: GET_SECRET_ACCESS_KEY = $$(aws ssm get-parameters --names $(GOV_SECRET_KEY_SSM) --with-decryption | jq -e -c -r '.Parameters[0].Value')
govprofile/create:
	@ echo "[$@]: Configuring local env to support GovCloud operations..."
	aws configure set aws_access_key_id $(GET_ACCESS_KEY_ID) --profile gov-dicelab-dev
	aws configure set aws_secret_access_key $(GET_SECRET_ACCESS_KEY) --profile gov-dicelab-dev
	aws configure set region us-gov-west-1 --profile gov-dicelab-dev
	aws configure list --profile gov-dicelab-dev
	aws sts get-caller-identity --profile gov-dicelab-dev --region us-gov-west-1
	@ echo "[$@]: Govcloud env configuration complete"

event: | govprofile/create
	@ if [ -n "$(IS_REVIEW)" ]; then \
		echo "[$@] REVIEW: Event is a pull request review..."; \
		echo "[$@] REVIEW: Checking whether a release will be triggered..."; \
		if $(MAKE) release/test; then \
			echo "[$@] REVIEW: Creating terraform plans for all account changes in version $(RELEASE_VERSION)..."; \
			$(MAKE) plan workingdir=*/*/base TB_TERRAGRUNT_COMMIT_RANGE=HEAD..$(PRIOR_VERSION); \
			echo "[$@]: Will release new version '$(RELEASE_VERSION)' when merged."; \
		else \
			echo "[$@]: NOTE: An error message is expected when release/test displays the same versions."; \
			echo "[$@] REVIEW: Creating terraform plans for changes in this pull request..."; \
			$(MAKE) plan workingdir=*/*/base TB_TERRAGRUNT_COMMIT_RANGE=HEAD..master; \
			echo "[$@]: Version has not incremented; no release will be triggered when merged."; \
		fi; \
	elif [ -n "$(IS_BRANCH)" ]; then \
		echo "[$@] BRANCH: Handling deploy for event on branch '$(IS_BRANCH)'..."; \
		$(MAKE) release/tag; \
	elif [ -n "$(IS_TAG)" ]; then \
		echo "[$@] TAG: Handling deploy for event on tag '$(IS_TAG)'..."; \
		echo "[$@] TAG: Applying configs to 'aws' and 'aws-us-gov' partitions..."; \
		$(MAKE) apply workingdir=*/*/base TB_TERRAGRUNT_COMMIT_RANGE=$(IS_TAG)..$$(git describe --abbrev=0 --tags $(IS_TAG)^); \
	elif [ -n "$(IS_SCHEDULE)" ]; then \
		echo "[$@] SCHEDULE: Handling scheduled build event '$(IS_SCHEDULE)'..."; \
		$(MAKE) plan workingdir=*/*/base TB_TERRAGRUNT_EXCLUDE="aws/518294798677/base" TF_CLI_ARGS_plan="-detailed-exitcode"; \
	else \
		echo "[$@] ERROR: Unknown event type!"; \
		exit 1; \
	fi
.PHONY: event

COMMANDS = validate plan apply
$(COMMANDS): WORKING_DIR := $(subst /, ,$(workingdir))
$(COMMANDS): partition := $(word 1,$(WORKING_DIR))
$(COMMANDS): partition := $(or $(partition), aws)
$(COMMANDS): account := $(word 2,$(WORKING_DIR))
$(COMMANDS): account := $(or $(account), *)
$(COMMANDS): config := $(subst $(space),/,$(wordlist 3,$(words $(WORKING_DIR)),$(WORKING_DIR)))
$(COMMANDS): config := $(or $(config), base)

apply: command = $@ $(options)

plan: command = $@ -var role_name=TECHREADONLY $(options)
plan: | validate

validate: command = $@ $(options)
validate: | prereq

# Create targets for all terraform COMMANDS
# These targets support two forms. Use one form OR the other. Mixing the forms
# results in undefined behavior.
#    make workingdir=<partition>/<account>/<config>
#    make partition=<partition> account=<account> config=<config>
$(COMMANDS): | guard/env/partition guard/env/config guard/program/zip guard/program/tb
	@ echo "[$@/$(partition)/$(account)/$(config)] Running '$@' for config '$(partition)/$(account)/$(config)'..."
	pipenv run tb terragrunt $(command) --include "$(partition)/$(account)/$(config)"
	@ echo "[$@/$(partition)/$(account)/$(config)] Command completed successfully!"
.PHONY: $(COMMANDS)

prereq: command = import -var role_name=TECHREADONLY $(options)
prereq: | guard/env/partition guard/env/config guard/program/zip guard/program/tb
	@ echo "[$@/$(partition)/$(account)/$(config)] Running '$@' target for config '$(partition)/$(account)/$(config)'..."
	pipenv run tb terragrunt $(command) module.iam_roles.aws_iam_role.prereq[0] AC2SPMGT --include "$(partition)/$(account)/$(config)" --no-err-exit --terragrunt-source-update
	pipenv run tb terragrunt $(command) module.iam_roles.aws_iam_role.prereq[1] TECHREADONLY --include "$(partition)/$(account)/$(config)" --no-err-exit
	@ echo "[$@/$(partition)/$(account)/$(config)] Target '$@' completed successfully!"
