# terrabroker

Terraform configurations for cloud environments managed by a Cloud Broker.

## One-time Workstation Setup

1. Configure AWS CLI credential.
2. Install git.
3. Configure git to use your AWS credential when working with CodeCommit in
   us-east-1:

       git config --system --remove-section credential
       git config --global --remove-section credential
       git config --global --remove-section 'credential.https://git-codecommit.us-east-1.amazonaws.com'
       git config --global credential.'https://git-codecommit.us-east-1.amazonaws.com'.helper '!aws codecommit credential-helper $@'
       git config --global credential.'https://git-codecommit.us-east-1.amazonaws.com'.UseHttpPath true

4. Clone this project from CodeCommit:

       git clone https://git-codecommit.us-east-1.amazonaws.com/v1/repos/terrabroker

## Prerequisites

1. Retrieve latest versions of required tooling and place them in your PATH:
   - **Git**: <https://git-scm.com/downloads>

## Create a New Account Configuration

> **NOTE**: Everything in this section can be completed on _any_ OS platform, OSX,
> Linux, Windows, etc.

First up, ensure you've completed the [Prerequisites](#prerequisites) and the
[One-time Workstation Setup](#one-time-workstation-setup).

Then:

01. Update your clone:

        cd terrabroker
        git checkout master
        git pull
02. Create a new branch, e.g. `git checkout -b <branch-name>`.
03. Open `./accounts.tfvars`, and add the new account Name/ID to the map. Be
    sure to enter the info under the correct AWS partition.
04. Copy the directory "`_templates/<partition>/tenant-dicelab`" to the
    partition directory, e.g. "`aws`" or "`aws-us-gov`".
05. Rename the copied directory to the Account ID of the new tenant.
06. Modify the file "`<partition>/<account-id>/base/account.auto.tfvars`" with
    information for this tenant account.
07. Commit the change:

        git add .
        git commit -m 'Adds account configuration for <account-name>'

08. If this is an existing legacy account, you will need to generate an import config
    **and** import the resources. For instructions, refer to the [import documentation](_terrabroker/README.md)
    for the Terrabroker python cli, `tb`.
09. Bump the `<patch>` token in the version file. See the section [Terrabroker Versioning](#terrabroker-versioning).
    In this case, it is ok to use the same branch used to add the new account
    configuration.
10. Push the new branch: `git push -u origin <branch-name>`.
11. Open a [Pull Request](https://console.aws.amazon.com/codecommit/home?region=us-east-1#/repository/terrabroker/pull-requests/new).

## Review a New Account Configuration

1. Review the [Pull Request in CodeCommit](https://console.aws.amazon.com/codecommit/home?region=us-east-1#/repository/terrabroker/pull-requests).
2. Spot check the new account configuration...
   - Does the Account ID in the folder match the Account ID in `accounts.tfvars`?
   - Is the account folder in the correct partition?
   - Are all the variables completed in `account.auto.tfvars`?
   - Are the VPC and subnet CIDRs correct? (For dicelab, VPC should be /16,
     private subnets /19, and public subnets /20)
   - Is the `terraform.tfvars` file present? Its contents must match what
     is in the partition's `_template` baseline.
   - Has the version been bumped correctly? Only the `<patch>` token should be incremented
     when adding a new account configuration.
3. Once open, the CI system will kick off a job that validates the config and runs
   a plan on the new account. Ensure the CI job succeeds. Review the plan output
   to ensure the actions are expected and understood.
4. Request changes if necessary.
5. Merge the Pull Request when everything looks good.

## Removing an Account

Occasionally, an account needs to be removed. The steps to remove an account
are fairly simple, just destroying the tenant resources that exist in the parent
account and deleting the information that was commited when adding the account:

1. Update your clone:

      cd terrabroker
      git checkout master
      git pull
2. Create a new branch, e.g. `git checkout -b <branch-name>`.
3. Destroy tenant resources that exist in the parent account.

        working_dir=<partition>/<account>/base
        terragrunt destroy \
          -target module.keystore \
          -target module.pcx.aws_route.peer \
          -target module.pcx.aws_vpc_peering_connection_accepter.this \
          -target module.route53_subzone.aws_route53_record.root_public_ns \
          -target module.iam_roles.aws_iam_role.parent \
          -target module.iam_roles.aws_iam_role_policy.parent \
          --terragrunt-working-dir $working_dir
4. Delete the directory `<partition>/<account-id>`
5. Remove the account record from `./accounts.tfvars`
6. Commit the change:

        git add .
        git commit -m 'Removes account configuration for <account-name>'
7. Bump the `<patch>` token in the version file. See the section [Terrabroker Versioning](#terrabroker-versioning).
   In this case, it is ok to use the same branch used to add the new account
   configuration.
8. Push the new branch: `git push -u origin <branch-name>`.
9. Open a [Pull Request](https://console.aws.amazon.com/codecommit/home?region=us-east-1#/repository/terrabroker/pull-requests/new).

## Apply an Account Configuration

Applying a terraform configuration meets several use cases:

- Create the initial set of resources in the account, according to the
  account baseline design at the time.
- As the baseline design is updated, deploy/modify resources in existing
  accounts to match the updated design.
- Check/enforce that the account resources and configurations continue to
  match what is defined in the baseline design.

In general, the apply step should be executed by CI. This is accomplished by bumping
the Terrabroker version and opening a pull request. Once merged, CI will create
the git tag for the release, and on the tag event CI will run the apply on any account
configs that were changed in the released version.

It is also possible to apply the configuration manually, but that is not recommended
except in extraordinary circumstances.

### Apply Account Configuration via CI

To apply an account configuration via the CI system, there are four steps:

- Bump the version
- Open a pull request
- Review the plan in the linked CI job
- Merge the pull request

1. The first step is to bump the version. See the section [Terrabroker Versioning](#terrabroker-versioning).

2. Open the [Pull Request in CodeCommit](https://console.aws.amazon.com/codecommit/home?region=us-east-1#/repository/terrabroker/pull-requests/new).

3. Review the pull request. The CI system will automatically launch a CodeBuild
   job to create a plan for all account configs that have changed in this release.
   Ensure the plan succeeds, and review the output to ensure that the changes are
   expected. Fix any minor problems in the same branch. If the problems are more
   significant, fix them in a separate branch and separate pull request first, then
   rebase the release branch.

4. Once all looks well in the pull request, merge it. The CI system will automatically
   launch a job that will create the git tag, and the tag event will kick off another
   job that will apply the terraform configs.

### Apply Account Configuration Manually

If you absolutely must, you can apply account configurations manually. However,
this should be reserved for extraordinary circumstances, and the version *still*
needs to be bumped as per the automated method.

1. Ensure that you have completed the [Prerequisites](#prerequisites) and that
   you have [installed the `tb` utility](_terrabroker/README.md).
2. Update your clone:

       cd terrabroker
       git checkout master
       git pull

3. Run a plan:

       make plan workingdir=<partition>/<account-id>/base

4. Review the plan output to ensure the actions are expected and understood.
5. Apply the plan:

       make apply workingdir=<partition>/<account-id>/base

6. Review the results. If there are problems then troubleshoot, correct the
   deficiency, and re-run the plan-apply steps.

## Naming Requirements for AWS CLI Profiles

When interacting with Terragrunt, such as when applying Terraform configurations
manually, Terrabroker relies on the profile setup of the AWS CLI shared configuration
and shared credential files. In particular, Terrabroker has very specific requirements
for *naming* the necessary AWS CLI profile/s.

These naming requirements **do not** apply when adding or modifying account
configurations. They apply **only** when actually using the Terraform or
Terragrunt utilities.

The naming requirements are based on the AWS partition.

### AWS CLI Profile Requirements

Terrabroker leverages the "AssumeRole" capability of the Amazon STS service to
authenticate to all tenant and payer accounts, in all partitions. As a result,
a single profile per partition needs to be defined in the AWS Shared
Configuration file (e.g. `~/.aws/config`) and the Shared Credential file
(e.g. `~/.aws/credentials`).

For the `aws` partition, the name of the profile must match the name of the
parent account, i.e.:

- `dicelab-dev`

For the `aws-us-gov` partition, the name of the profile must additionally be
preceeded by the identifier `gov-`, i.e.:

- `gov-dicelab-dev`

The profile name must be specified as all lower-case, and any underscores (`_`)
must be replaced with a dash (`-`).

The credential associated with this profile must have permissions to assume the
role named in the partition's `<parent>.tfvars` file, i.e. `E_ADMINISTRATOR`.
The specified role must pre-exist in the target/tenant account.

## Importing a "Legacy" Account

Terrabroker supports importing resources that are not yet managed by Terraform!

First create the Terrabroker config for the account as described above, in
*Create a New Account Configuration*.

Then refer to the [import documentation](_terrabroker/README.md) for the Terrabroker
python utility, `tb`, to import existing resources that are not yet managed by
Terrabroker.

## Terrabroker Versioning

Terrabroker uses semantic versioning, following the pattern `<major>.<minor>.<patch>`.
Here are some criteria for which token to bump:

- *Patch*
  - New account added to Terrabroker.
  - Edit to an existing account's variables.
- *Minor*
  - Change to the account module version in terraform.tfvars in all accounts.
- *Major*
  - A backwards incompatible change was made to the project structure that significantly
    changes the resource addresses in the Terraform state file. Requires significant
    work to convert managed accounts.

The version is controlled using the `.bumpversion` file in the project root. The
file can be edited manually, or using the python utility `bumpversion`.

As always when editing content in a git repo, first create a branch, then edit the
file.

    git checkout -b <version>
    # edit the file manually and create the commit
    #     git add .bumpversion
    #     git commit -m "Bumps version to <version>"
    # or use bumpversion
    #     bumpversion patch
    git push -u origin <version>

Then open a pull request to review and merge the change.
