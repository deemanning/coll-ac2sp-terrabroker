terragrunt = {
  remote_state {
    backend = "s3"

    config {
      profile        = "ac2sp-iam"
      region         = "us-gov-west-1"
      bucket         = "ac2sp-prod-cb-terrabroker-tfstate"
      key            = "tfstate/${path_relative_to_include()}/terraform.tfstate"
      encrypt        = true
      dynamodb_table = "ac2sp-prod-cb-terrabroker-tfstate-lock"
    }
  }

terraform = {
    extra_arguments "common" {
      commands = ["${get_terraform_commands_that_need_vars()}"]

      arguments = [
        "-var-file",
        "${get_parent_tfvars_dir()}/../../accounts.tfvars",
      ]
    }

    extra_arguments "account" {
      commands = ["${get_terraform_commands_that_need_vars()}"]

      arguments = [
        "-var-file",
        "${get_tfvars_dir()}/account.auto.tfvars",
      ]
    }
  }
}
