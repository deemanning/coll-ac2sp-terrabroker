terragrunt = {
  # Include all settings from the root terraform.tfvars file
  include = {
    path = "${find_in_parent_folders()}"
  }

  terraform {
    source = "../../../_roots/prereq"

    after_hook "provider" {
      commands = ["init-from-module"]
      execute  = ["cp", "${get_tfvars_dir()}/../../ac2sp-prod.tf", "."]
    }

    extra_arguments "enclave" {
      commands = ["${get_terraform_commands_that_need_vars()}"]

      arguments = [
        "-var-file",
        "${get_tfvars_dir()}/../../ac2sp-prod.tfvars",
      ]
    }

    extra_arguments "account" {
      commands = ["${get_terraform_commands_that_need_vars()}"]

      arguments = [
        "-var-file",
        "${get_tfvars_dir()}/../base/account.auto.tfvars",
      ]
    }
  }
}
